This project folder is exclusively for issues for:

- [Onboarding](.gitlab/issue_templates/onboarding.md)
- [Offboarding](.gitlab/issue_templates/offboarding.md)
- [Internal Transition](.gitlab/issue_templates/internal_transition.md)