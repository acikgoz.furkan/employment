1. [ ] People Ops: Once the termination has taken place (whether voluntary or involuntary), as soon as possible, create a **confidential** issue called 'Offboarding (NAME), per (DATE, please follow yyyy-mm-dd)' in in the [People Ops Employment Issue Tracker](https://gitlab.com/gitlab-com/people-group/employment/issues) with relevant lines of the master offboarding checklist.
1. [ ] People Ops: Enter the former team member's GitLab email address and handle below.

|||
| ----------------- | ---------------------- |
| GitLab E-mail     | `{email}` |
| GitLab.com Handle | `{@handle}`|

## All GitLab Team Members

1. [ ] For this offboarding, the manager is @MENTION, and People Ops is handled by @MENTION. Assign this issue to them.
1. [ ] People Ops: Add a due date to this issue exactly 30 days from the last day of their employment. We will use this as a reminder to delete their GSuite account.

## BambooHR and Okta



<summary>People Ops</summary>

1. [ ] People Ops: In a private message with the People Business Partner in charge of the termination, confirm the Termination Type, Termination Reason and if the team member is Eligible for Rehire. 
1. [ ] People Ops: At the far right corner of the team member's profile, click on the gear icon, choose `Terminate Employee`. Use the date mentioned in this issue as final date of employment / contract. Enter Termination Type, Termination Reason and Eligible for Rehire per the answers received from the People Partner.
1. [ ] People Ops: If a team member is terminated before their Probationary Period has completed, delete the "Active" line dated in the future in the Employment Status section of BambooHR.
1. [ ] People Ops : Okta entitlements are driven based on BambooHR status. Therefore to remove the user in Okta, the best way to set off that workflow is to force an Import from BambooHR into Okta. Log into the [Okta dashboard](https://gitlab-admin.okta.com/), go to the BambooHR Admin application in the Applications Menu. Go to the Import Tab and click the Import Now button. This will force an import, and you should see a message that a user has been removed. Note that this will also trigger any deprovisioning workflows for Applications that Okta is configured to perform Deprovisioning on, as well as disable the user's Okta account.



## Google account


<summary>People Ops</summary>

1. [ ] People Ops: Log on to the [Google Admin console](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx) and find the team member's profile.
1. [ ] People Ops: Click the `More` button (left hand side of screen) and then click `Suspend User` and click it again to confirm. This will ensure that the user can no longer access their account. 
1. [ ] People Ops: Rename the team member's email from `username@gitlab.com` to `former_username@gitlab.com` and delete the original email as an alias.
1. [ ] People Ops: Following the [instructions in the handbook](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/), set an automatic rejection rule. This sends an auto-response to the sender notifying them that the team member is no longer with GitLab and who to contact instead.



## GitLab Accounts


<summary>Manager</summary>

1. [ ] Manager: Review all open merge requests and issues assigned to the team member and reassign them to you or another team member as per your handover agreement within the team.




<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : Check if former GitLab Team Member had a gold account on GitLab, if so contact support to deactivate it.
      - You can do this by searching [dotcom-interal](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=GitLab%20Gold%20Request) for Closed `~"GitLab Gold Request"`s. You can use the `author` filter to narrow it down.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Remove former GitLab Team Member's' GitLab.com account from the [gitlab-com group](https://gitlab.com/groups/gitlab-com/group_members).
   1. [ ] IT Ops @gitlab-com/business-ops/itops : Block former GitLab Team Member, remove from all company Groups and Projects, and then unblock. Remove GitLab job role if visible on the profile. Make sure to change their associated email address to their personal email address.
   1. [ ] IT Ops @gitlab-com/business-ops/itops : Remove former GitLabber's admin account, if applicable.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Block former GitLab Team Member's [dev.GitLab.org account](https://dev.gitlab.org/admin/users) and remove from [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members).
1. [ ] IT Ops @gitlab-com/business-ops/itops : Block former GitLab Team Member's [staging.GitLab.com account](https://staging.gitlab.com/admin/users) and remove from [gitlab group](https://staging.gitlab.com/groups/gitlab/group_members).



## Slack


<summary>People Ops</summary>

1. [ ] People Ops: [Check if the team member has created any Slack bots](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/) before disabling account. [Link](https://gitlab.slack.com/apps/manage?utm_source=in-prod&utm_medium=inprod-apps_link-slack_menu-click) to Apps menu.
1. [ ] People Ops: Disable team member in [Slack](https://gitlab.slack.com/admin). If the team member is an Admin ping Erich or Sid to remove them.
1. [ ] People Ops: If a voluntary termination, add team member as a single channel guest to `gitlab-alumni` with the personal email found in BambooHR.




## 1Password



<summary>People Ops</summary>

1. [ ] People Ops: Log into [1Password](https://1password.com/) and click on "People" in the right-hand sidebar. Search for the team member's name. Click "More Actions" under their name and choose "Suspend" to remove access to 1Password. Take a screenshot of the user's permissions and post it as a comment in this offboarding issue.




<summary>Manager</summary>

1. [ ] Coordinate with IT Ops @gitlab-com/business-ops/itops to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
    1. [ ] Review if team member had sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
    1. [ ] Review what 1Password vaults former team member had access to, and identify any shared passwords to be changed and moved to Okta.




## Calendars & Agenda



<summary>People Ops</summary>

1. [ ] People Ops: Remove team member from [Breakout Rooms spreadsheet](https://docs.google.com/spreadsheets/d/1M-6NjK8_gAqcU1BehQVkKuDa1Ua2hTGBy8TuEDXHh3g/edit#gid=0).




<summary>Manager</summary>

1. [ ] Manager: Add entry to Team Call agenda to announce departure of team member: 'X is no longer with GitLab'. Do not say anything beyond that for involuntary terminations, most people will have seen the general channel announecment.
1. [ ] Manager: Remove team member from team meeting invitations.
1. [ ] Manager: Cancel weekly 1:1 meetings with team member.




## Notifications



<summary>People Ops</summary>

1. [ ] People Ops: Notify Security of the offboarding (@jurbanc).
1. [ ] People Ops: Notify CFO (@pmachle) and Sr Stock Administrator (@tdominique)of offboarding. If this team member is on garden leave, please ensure the exact dates and process is shared with both the CFO and Sr Stock Administrator.
1. [ ] People Ops: Notify Marketing (@jjcordz) to remove from social/Twitter/Tweekdeck (GitHostIO, gitlabstatus, GitLabSupport) if applicable.




<summary>IT Ops</summary>

1. [ ] IT Ops: Reach out to former team member to identify and retrieve any company supplies/equipment. @gitlab-com/business-ops/itops See the [Offboarding page](https://about.gitlab.com/handbook/offboarding/) for further details on that process.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Comment in this issue when equipment has been returned/recycled. Ping hiring manager, Security and `@llamb`.
1. [ ] IT Ops @gitlab-com/business-ops/itops : Inform Controller / Accounting if any items in former GitLab Team Member's possession will not be returning, so that they can be removed from asset tracking.


<summary>Manager</summary>

1. [ ] Manager: For VP and above terminations, announce in #e-group-confidential chat channel, prior to the #team-member-update announcement, including brief context for the termination.
1. [ ] Manager: Announce in #team-member-updates chat channel as soon as the chat and Google Accounts are blocked: 'As of today, XX is no longer with GitLab. I would like to take this opportunity to thank XX for their contributions and wish them all the best for the future. If you have questions about tasks or projects that need to be picked up, please let me know. If you have concerns, please bring them up with your manager.' Note, for VP and above positions, that manager will provide brief context for the termination.
1. [ ] Manager: if the team member previously made an announcement about leaving, copy the link to the message/mention in the Team Agenda or refer them to the actual team call announcement itself. It is very important to send this message as soon as possible so people know that they can rely on official communication channels and not have to find out through the grapevine. Delays in announcing it are not acceptable. The policy of not commenting on circumstances is in force indefinitely, even if the termination is voluntary. It is unpleasant, but it is the right thing to do. If people press for answers say you don't want to suggest that underperformance was a reason for this exit but remind them that:
1. [ ] Manager: Organize smooth hand over of any work or tasks from former team member. Offer option to send a message to everyone in the company (i.e. forwarded by the manager), for a farewell message and/or to transmit their personal email address in case people wish to stay in touch.




<summary>Accounting & Finance</summary>

1. [ ] Accounting (@llamb): Do not approve any submitted or in progress Expensify expenses until there is confirmation of return of laptop.
1. [ ] Accounting (@llamb): Cancel company American Express card if applicable.
1. [ ] Accounting (@nprecilla): Remove team member from Expensify.
1. [ ] Finance (@wwright): Remove team member from [Rolling 4 Quarter GitLab Team Members](https://docs.google.com/spreadsheets/d/12mNijMwA8hIG5h3zV5JJ0oxsVh6xzk6-si0eT7L9mvM/edit#gid=828135525) spreadsheet.



## Handbook & Team Page


<summary>Manager</summary>

1. [ ] Manager: Edit team member's [team page](https://about.gitlab.com/team) entry to display as a vacancy if applicable. If position is not to be backfilled, delete the entire entry.




<summary>People Ops</summary>

1. [ ] People Ops: Remove team member's profile picture on the [team page](https://about.gitlab.com/team). Important! Before deleting the image, make sure that you edit the team.yml entry for the team member with the picture line as `picture: ../gitlab-logo-extra-whitespace.png`
1. [ ] People Ops: If the team member is a people manager at GitLab, be sure to remove their slug from their direct reports profiles on the team page as well. If a new manager is not yet assigned, please replace the manager slug with the departing GitLab team member's direct manager. (I.E. If the departing team member with the title Manager, GitLab reports to Director, GitLab then Director, GitLab should be listed as the manager for Manager, GitLab's direct reports)
1. [ ] People Ops: If applicable, remove team member's pet(s) from [team pets page](https://about.gitlab.com/team-pets). Don't forget to remove the picture(s).
1. [ ] People Ops: Remove hard-coded reference links of the team member from our documentation and handbook (i.e. links to the team page) by following the [instructions on the handbook](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/)
1. [ ] People Ops: Paste the links to the MRs in the comments of the offboarding issue for visibility.


## Other Systems and Tools


<summary>People Ops</summary>

1. [ ] People Ops: Okta should now deprovision Zoom as part of Okta Deactivation, please verify it was deactivated. If not, please ping the team in the #okta slack channel
1. [ ] People Ops: Delete account in [Moo](https://moo.com).
1. [ ] People Ops (Analyst Coordinator @julie.samson): Remove team member from compensation calculator
1. [ ] People Ops (Analyst Coordinator @julie.samson): Ensure there is no outstanding tuition reimbursement that would need to be refunded to GitLab.
1. [ ] People Ops (@ewegscheider): Deactivate former team member from [Greenhouse](https://app2.greenhouse.io/dashboard#).

<summary>Manager</summary>

1. [ ] Manager: Reach out to [TripActions Support](https://app.tripactions.com/app/user/search) to request future (non-personal) trips booked are canceled. 


<summary>Business Systems Analyst</summary>

1. [ ] Business Systems Analyst (@lisvinueza): Review and confirm if team member is a system admin/provisioner.
1. [ ] Business Systems Analyst (@lisvinueza): If team member is a system admin/provisioner, open and complete an [Update Tech Stack Provisioner issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Update%20Tech%20Stack%20Provisioner).



## Tech Stack System Deprovisioning 

#### System Admins

@tech-stack-provisioner
Review the table of applications listed below that is included in our [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0). For applications that you Own/Admin, please review and ensure the former team member does **not** have access to your system(s).

**If the user DOES have access to a system you administer access to:**
- Review and remove the former team member's access.
- Select the applicable checkbox(es) once access has been removed from the impacted system.

**If the user DOES NOT have access to a system you administer access to:**
- After confirming the former team member does not have access to the system, select the checkbox indicating access has been deprovisioned.


<summary>Alex Tkach</summary>

1. [ ] GovWin IQ
1. [ ] LicenseApp
1. [ ] Sertifi


<summary>Amber Stahn</summary>

1. [ ] Datafox
1. [ ] Salesforce
1. [ ] Clari




<summary>Ann Tiseo/Jenny Nguyen</summary>

1. [ ] ADP




<summary>Anthony Carella</summary>

1. [ ] Tenable.IO




<summary>Christie Lenneville, Taurie Davis, Sarah O'Donnell</summary>

1. [ ] Optimal Workshop




<summary>Cindy Nunez</summary>

1. [ ] Blackline
1. [ ] Tipalti




<summary>David Sakamoto</summary>

1. [ ] LucidChart




<summary>David Smith, Anthony Sandoval, Brent Caldwell</summary>

1. [ ] AWS (production)
1. [ ] AWS (support/staging)
1. [ ] AWS (gitter)
1. [ ] AWS (gov cloud)
1. [ ] AWS (gitlab.io)
1. [ ] Azure
1. [ ] Elastic Cloud
1. [ ] Fastly CDN
1. [ ] GitLab.com Prod/staging rails and db console (ssh)
1. [ ] customers. -gitlab.com (ssh)
1. [ ] forum. -gitlab.com (ssh)
1. [ ] license. -gitlab.com (ssh)
1. [ ] version. -gitlab.com (ssh)
1. [ ] ops.gitlab.net
1. [ ] Google Cloud Platform
1. [ ] Grafana (dashboards.gitlab.net)
1. [ ] Sentry (sentry.gitlab.net)
1. [ ] PackageCloud
1. [ ] Rackspace
1. [ ] Status - IO




<summary>Erich Wegscheider</summary>

1. [ ] ContactOut - Sourcing Team only
1. [ ] DocuSign
1. [ ] LinkedIn Recruiter




<summary>Jessica Mitchell/Jacie Bandur</summary>

1. [ ] Will Learning




<summary>JJ Cordz</summary>

1. [ ] Bizible
1. [ ] Cookiebot
1. [ ] DiscoverOrg
1. [ ] Disqus
1. [ ] Drift
1. [ ] Eventbrite
1. [ ] Facebook Ad Platform
1. [ ] FunnelCake
1. [ ] Google Adwords
1. [ ] Google Analytics
1. [ ] Google Tag Manager
1. [ ] Mandrill
1. [ ] Marketo
1. [ ] Mailchimp
1. [ ] Meetup
1. [ ] Moz Pro
1. [ ] Shopify
1. [ ] Sigstr
1. [ ] Tweetdeck
1. [ ] WebEx
1. [ ] YouTube
1. [ ] Chorus
1. [ ] LeanData
1. [ ] Google Search Console
1. [ ] MailGun
1. [ ] Sprout Social
1. [ ] LinkedIn Sales Navigator
1. [ ] Outreach




<summary>Liam McAndrew</summary>

1. [ ] CrowdIn - Lower the team member's permissions on [Crowdin](https://translate.gitlab.com/project/gitlab/settings#members) to translator




<summary>Lis Vinueza</summary>

1. [ ] Zendesk Light Agent




<summary>Lukas Eipert</summary>

1. [ ] JetBrains - Revoke JetBrains licenses. Go to the [user management](https://account.jetbrains.com/organization/3530772/users) and search for the team member, revoke their licenses.




<summary>Matt Benzaquen, Swetha Kashyap, Wilson Lau</summary>

1. [ ] Xactly




<summary>Nichole LaRue, JJ Cordz</summary>

1. [ ]	PathFactory




<summary>Paul Machle</summary>

1. [ ]	Carta




<summary>People Ops</summary>

1. [ ]	Hello Sign




<summary>Robert Nalen</summary>

1. [ ]	Conga Contracts
1. [ ]	ContractWorks
1. [ ]	Visual Compliance




<summary>Sarah O’Donnell</summary>

1. [ ]	Mural
1. [ ]	Qualtrics




<summary>Tony Carter</summary>

1. [ ]	TheHive




<summary>Shaun McCann</summary>

1. [ ]	Unbabel




<summary>Taylor Murphy, Justin Stark, Kathleen Tam</summary>

1. [ ]	Periscope
1. [ ]	Snowflake
1. [ ]	Stitch
1. [ ]  Fivetran




<summary>Wilson Lau, Cristine Marquardt</summary>

1. [ ]	Avalara
1. [ ]	Stripe
1. [ ]	Zuora



### For GitLab Inc employees only


<summary>People Ops</summary>

1. [ ] People Ops: Notify payroll specialist (@hdevlin) of termination.



### For GitLab BV Belgium only


<summary>People Ops</summary>

1. [ ] People Ops: Once the termination date is known verify with COO or CEO that the non-competition clause is to be waived or enforced.
1. [ ] Non-competition waived: People Ops: send a letter (in French or Dutch depending on location in Belgium) via registered mail and by email to the team member within 15 days of the termination date.
1. [ ] Non-competition enforced: People Ops: to inform Financial Controller & instruct payroll to pay the team member a lump sum as stated in the contract.
1. [ ] People Ops: Inform payroll of last day and ask them to confirm how many ecocheques the person is due and when they will be issued. Payroll will also issue a vacation certificate which is required to be given to the next employer.



### For GitLab BV employees only


<summary>People Ops</summary>

1. [ ] People Ops: Remove former team member HRSavvy, inform co-employer payrolls.



### For GitLab LTD employees only


<summary>People Ops</summary>

Note: All the below should be done in  one email.
1. [ ] People Ops: Inform Vistra payroll team (1Password -> PeopleOps vault -> Entity Entity & Co-Employer HR Contacts note) of the termination effective date and instruct them to send the individual's P45 to their home address.
1. [ ] People Ops: Notify Vistra HR team that if employee was enrolled into the pension scheme advise payroll of the last day of employment. Ask for confirmation when this is completed.
1. [ ] People Ops: Notify Vistra HR team to remove employee from the Scottish Widows pension scheme (if applicable). Ask for confirmation when this is completed.
1. [ ] People Ops: Notify Vistra HR team to remove employee from the medical insurance effective their termination date. Ask for confirmation when this is completed.



### For GitLab GmbH employees only

 
<summary>People Ops</summary>

1. [ ] People Ops: Inform payroll of last day of employment.

 


### For GitLab BV Lyra only

 
<summary>People Ops</summary>

1. [ ] People Ops: Inform Lyra HR of the last day of employment.

 

### For CXC Employees and Contractors

 
<summary>People Ops</summary>

1. [ ] People Ops: For voluntary terms, contact the appropriate CXC representative of the last day of employment (contact details are in People Ops 1Password Vault).

 

### For SafeGuard Employees

 
<summary>People Ops</summary>

1. [ ] People Ops: For voluntary terms, contact the appropriate SafeGuard representative of the last day of employment.

 

### FOR ENGINEERING ONLY (Devs, PEs, SEs)

 
<summary>Manager</summary>

1. [ ] Manager: Remove former GitLab Team Member's' GitHub.com account from the [gitlabhq organization](https://github.com/orgs/gitlabhq/people) (if applicable).
1. [ ] For former Developers (those who had access to part of the infrastructure), and Production GitLab Team Members: copy offboarding process from [infrastructure](https://dev.gitlab.org/cookbooks/chef-repo/blob/master/doc/offboarding.md) for offboarding action.
1. [ ] Manager: Remove access to PagerDuty if applicable.
1. [ ] Manager (For Build Engineers): Remove team member as a member to the GitLab Dev Digital Ocean account https://cloud.digitalocean.com/settings/team.
1. [ ] Manager: Remove HackerOne credentials if applicable.
1. [ ] Manager: Remove from GitLab Docker Hub teams if applicable.
2. [ ] Manager: Remove from any [on-call obligations](https://docs.google.com/spreadsheets/d/10uI2GzqSvITdxC5djBo3RN34p8zFfxNASVnFlSh8faU/edit#gid=1066364624), and notify coordinator

 

 
<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : Remove any development VMs. Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).

 

### FOR SUPPORT ENGINEERING/AGENTS ONLY

 
<summary>Manager</summary>

1. [ ] Manager: Check and remove access to legacy accounts.
   1. [ ] Manager: Legacy - hackerone.com.
1. [ ] Zendesk [(general information about removing agents)](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2):
   1. [ ] Manager: Remove any triggers related to the agent - https://gitlab.zendesk.com/agent/admin/triggers.
   1. [ ] Manager: Downgrade the agent role to "end-user" - [more information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2).
        + **Warning: This will unassign all tickets from the agent** Consider reducing the "full agent" count on our Zendesk license.
   1. [ ] Manager: Schedule a date to suspend the agents account. [More information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#3).
1. [ ] Community Forum:
   1. [ ] Manager: Remove team member from "moderators" group on the [GitLab community forum](https://forum.gitlab.com/).
1. [ ] Hiring Manager: Downgrade GitHost.io account to user privileges - [Set `user_type` to `0`](https://dev.gitlab.org/gitlab/GitHost#create-a-new-admin-user).

 
 
 
<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : Remove any development VMs. Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).
1. [ ] IT Ops @gitlab-com/business-ops/itops : Revoke GitLab.com admin access if they had it. If you're not sure (i.e. their access predates access requests) ping the manager.

 


### FOR UX DESIGNERS, FRONTEND DEVS, AND DESIGNERS ONLY

 
<summary>Manager</summary>

1. [ ] Manager: Remove [SketchApp](http://www.sketchapp.com/) license via Sketch License Manager.
1. [ ] Manager: (for UX Designers) Remove former team member's `Master` access to the [gitlab-design](https://gitlab.com/gitlab-org/gitlab-design) project on GitLab.com.
1. [ ] Manager: (for UX Designers) Remove former team member from the [GitLab Dribbble team](https://dribbble.com/gitlab).
1. [ ] Manager: (for UX Designers) Remove former team member from [Balsamiq Cloud](https://balsamiq.cloud).




<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/itops : (for UX Designers) remove team member from the `@uxers` User Group on Slack.
1. [ ] IT Ops @gitlab-com/business-ops/itops : (for Designers only, not UX Designers) Remove access to [Adobe Creative Cloud](https://www.adobe.com/creativecloud.html) using the shared credential in the Secretarial vault.



### FOR UX RESEARCHERS ONLY


<summary>Manager</summary>

1. [ ] Manager: Remove former team member from [Balsamiq Cloud](https://balsamiq.cloud)
1. [ ] Manager: Remove former team member from [SurveyMonkey](https://surveymonkey.net)
1. [ ] Manager: Remove former team member from [UsabilityHub](https://usabilityhub.com)
1. [ ] Manager: Remove former team member from [MailChimp](https://mailchimp.com/)



### FOR MARKETING ONLY

* [ ] Marketing Ops: Remove from Tweetdeck.



<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
      * [ ]   Salesforce - @Astahn/@jjcordz (whoever is available): Immediately freeze the user if you cannot permanently deactivate the user. There may be a few reasons why you cannot deactivate the user (lead routing, etc).
      * [ ]   Salesforce - @Astahn: REASSIGN RECORDS in Salesforce: Export all leads, accounts, contacts, and open opportunities OWNED by the former team member and save as a flat file or googlesheets. Name the file `LASTNAME-FIRSTNAME-OBJECT-YYYY-MM-DD` (Object will be LEADS, ACCOUNTS, CONTACTS, or OPPORTUNITIES). Then reassign all leads, accounts, contacts, and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!) to their Manager.
      * [ ]   Salesforce - @Astahn: REMOVE FROM PICKLISTS: remove from SDR or BDR picklists on ACCOUNT and OPPORTUNITY objects and if applicable, replace the former SDR/BDR to new SDR/BDR on both the account and opportunity objects. Please do not replace the SDR/BDR CLOSED WON/LOST OPPS and only replace opportunities with a new SDR/BDR if the opportunity is not yet qualified.
      * [ ]   Chorus - @Astahn: Disable user and deactivate license.
      * [ ]   Outreach - @jjcordz: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ]   DiscoverOrg - @jjcordz: First, remove from DiscoverOrg Permission Set. Second, deactivate the user in DiscoverOrg.com.
      * [ ]   Marketo - @jjcordz: Remove from Workflow Campaigns, pick lists, and SFDC assignment sync.
      * [ ]   LeanData - @jjcordz: Remove from any lead routing rules and round robin groups.
      * [ ]   LinkedIn Sales Navigator - @jjcordz: Disable user, remove from team license.
      * [ ]   Drift - @jjcordz: IF User: Disable access; IF Admin: Reset password.
      * [ ]   1Password - @jjcordz: Rotate any shared login credentials (SFDC, Google Analytics, Adwords, Disqus, Facebook, LinkedIn, Zendesk).



### FOR SALES AND FINANCE ONLY


<summary>Finance</summary>

1. [ ] Finance: Remove from Comerica, Rabobank or any other account (as user or viewer only if in Finance).
1. [ ] Finance: Remove from Netsuite [QuickBooks users](https://about.gitlab.com/handbook/hiring/) (Finance only).




<summary>Manager</summary>

1. [ ] Manager: Remove from sales meeting.




<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
      * [ ]   Salesforce - @Astahn/@jjcordz (whoever is available): Immediately freeze the user if you cannot permanently deactivate the user. There may be a few reasons why you cannot deactivate the user (lead routing, etc).
      * [ ]   Salesforce - @Astahn: DISABLE only after Xactly payroll has been completed. Convert use license type from "Salesforce" to "Salesforce Platform".
      * [ ] RECORD Snapshot of Salesforce: Export all leads, accounts, contacts, and open opportunities OWNED by the former team member and save as a flat file or googlesheets. Name the file `LASTNAME-FIRSTNAME-OBJECT-YYYY-MM-DD` (Object will be LEADS, ACCOUNTS, CONTACTS, or OPPORTUNITIES).
      * [ ] REASSIGN all leads, accounts, contacts, and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!) based on direction provided by RD/VP within 24 hours of termination. Any named accounts will be transitioned either to RD or designated SAL/AM.
      * [ ]   Outreach - @jjcordz: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ]   DiscoverOrg - @jjcordz: First, remove from DiscoverOrg Permission Set. Second, deactivate the user in DiscoverOrg.com.
      * [ ]   Marketo - @jjcordz: Remove from Workflow Campaigns, pick lists, and SFDC assignment sync.
      * [ ]   LeanData - @jjcordz: Remove from any lead routing rules and round robin groups.
      * [ ]   Chorus - @Astahn: Disable user and deactivate license.
      * [ ]   LinkedIn Sales Navigator - @jjcordz: Disable user, remove from team license.
1. [ ] Rubén or Oswaldo: Remove from admin panel in the [Subscription portal](https://customers.gitlab.com/admin).

<summary>SALs and Named Account owners</summary>

<summary>For Planned Offboarding</summary>

1. [ ] @Astahn: After notice of termination, SalesOps to work with RD to identify new account and opportunity owner(s). This should happen between termination notice and termination date.
1. [ ] @Astahn: SalesOps/RD agree to status of Named Accounts. Should these be reassigned to territory owner or another Named Account AE? If former, uncheck Named Accounts; If latter, keep Named Accounts checked.
1. [ ] @jjcordz: Update Territory Model in LeanData with temporary territory assignments.
1. [ ] @jjcordz: Handbook should be update with temporary territory assignments.
1. [ ] @Astahn: Once backfilled or transitioned from existing rep, follow that process.

<summary>For Immediate Offboarding</summary>

1. [ ] @Astahn: Lock account - SFDC, Clari, Chorus, Outreach, DataFox, DiscoverOrg, LISN (if applicable); disable SFDC only after Xactly payroll has been completed. Convert User License type from “Salesforce” to “Salesforce Platform”.
1. [ ] @Astahn: Identify name, term dates, and territories. This comes from PeopleOps.
1. [ ] @Astahn: SalesOps to work with RD to identify new account and opportunity owner(s). This should happen within 24 hours of termination.
1. [ ] @Astahn: SalesOps/RD agree to status of Named Accounts. Should these be reassigned to territory owner or another Named Account AE? If former, uncheck Named Accounts; if latter, keep Named Accounts checked.
1. [ ] @Astahn: Update SDRs if applicable.
1. [ ] @jjcordz: Update Territory Model in LeanData with temporary territory assignments.
1. [ ] @jjcordz: Handbook should be update with temporary territory assignments.
1. [ ] @Astahn: Once backfilled or transitioned from existing rep, follow that process.




### FOR PEOPLE OPS ONLY


<summary>People Ops</summary>

1. [ ] People Ops (@brittanyr): Remove team member from BambooHR as an admin.
1. [ ] People Ops (@jkalimon): Remove team member from Cultureamp.




<summary>Manager</summary>

1. [ ] Manager: Remove team member from HR Savvy as an admin.



### FOR CORE TEAM MEMBERS ONLY


<summary>IT Ops</summary>

1.  [ ] IT Ops @gitlab-com/business-ops/itops : Remove developer access for [gitlab-org](https://gitlab.com/groups/gitlab-org).
1.  [ ] IT Ops @gitlab-com/business-ops/itops : Ping the [Code Contributor Program Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) to remove the former Core Team member as a `developer` in the [Core Team group](https://gitlab.com/groups/gitlab-core-team/-/group_members) and add the person to the `alumni.yml` file.



/confidential

/label ~offboarding ~"laptop request" ~"LaptopOffboarding::To Do"
