#### Core Team Members

<details>
<summary>Core Team Member</summary>

1. [ ] Core Team Member: Add yourself to the team page by following the instructions in [the handbook](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page), and make sure to include your nickname when you do so if you prefer to be called anything other than your first name. Most of the information including your role & department should have been filled out already, but please add your `story`.
1. [ ] Core Team Member: To support you in your contributions, you are able to [request a license for a JetBrains product](https://about.gitlab.com/handbook/tools-and-tips/#jetbrains).

</details>

<details>
<summary>People Ops</summary>

1. [ ] People Ops: Have member sign [NDA](https://about.gitlab.com/handbook/contracts/#core-team-nda) before any of the following steps are taken.
1. [ ] People Ops: Ping the [Code Contributor Program Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) to add member as a `developer` in the [Core Team group](https://gitlab.com/groups/gitlab-core-team/-/group_members)

</details>

<details>
<summary>Manager (Code Contributor Program Manager)</summary>

1. [ ] Manager: File request access for all Slack channels.
1. [ ] Manager: File request access for [gitlab-org](https://gitlab.com/groups/gitlab-org/group_members).

</details>