Instructions: People Opertions Specialist to get the code for swag in 1Password and replace the XXX below with the code before emailing

Subject: New Hire Swag 

Hi PERSON, 

Congrats on becoming a GitLab Team Member! We are so excited to have you joining our team, as a new employee we would like to share some GitLab swag with you!  Follow the directions below to get started. 

1. Please go to the [GitLab Swag Store](https://shop.gitlab.com/)
1. Choose some goods for yourself!
1. At checkout, use the code XXX which will take $25USD off your order.
1. Shipping is free!
1. Please remember to use your GitLab email at checkout on the contact information page.

If your chosen item or size is out of stock, please keep checking back, as your code does not expire.

Please let me know if you have any questions!

Best regards,