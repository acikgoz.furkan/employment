#### For People Ops Only

<details>
<summary>Manager</summary>

1. [ ] Manager: Invite team member to the `people-group-confidential` channel in Slack.

</details>


<details>
<summary>People Ops</summary>

1. [ ] People Ops (@ewegscheider): Add team member to Greenhouse as "Job Admin: People Ops".
1. [ ] People Ops: Ping `@tknudsen` or `@shaynes13` in this issue to add team member to the calendar invite for the PeopleOps team meetup at Contribute March 2020.

</details>
