### For employees in Germany only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Once you have received the employee questionnaire via HelloSign please fill in the missing details and sign.

</details>

<details>
<summary>People Ops</summary>

1. [ ] People Ops: Double check that the `Employment Status` in BambooHR has two entries marked as `Probation Period` with date as the hire date and `probation period ends` with the end date as exactly 6 months from the hire date.
1. [ ] People Ops: Prepare the GitLab GmbH Employee Questionnaire, found in HelloSign as a template. Go to HelloSign - Documents - Templates - Use Template. Enter the requested information. Stage for signature to the team member's personal email and PeopleOps assigned to the team member second. 
1. [ ] People Ops: Once the questionnaire has been completed and signed by the new team member, file the document in the new team member's BambooHR profile under Documents => Payroll Forms.
1. [ ] People Ops: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Ops: Send a copy of the signed employment contract and completed employee questionnaire, both encrypted to RPI. You can find their contact information in the PeopleOps vault in 1Password in the Entity & Co-Employer HR Contacts note.
1. [ ] People Ops: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>