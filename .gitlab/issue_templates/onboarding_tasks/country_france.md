<details>
<summary>New Team Member</summary>

1. [ ] People Ops: Double check that the `Employment Status` in BambooHR has two entries marked as `Probation Period` with date as the hire date and `probation period ends` with the end date as exactly 1-3 months from the hire date. Check contract for exact time.

</details>
