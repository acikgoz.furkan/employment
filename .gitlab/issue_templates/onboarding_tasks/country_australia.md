### For employees in Australia only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please provide the following to payroll@gitlab.com: [Tax file number declaration](https://www.ato.gov.au/uploadedFiles/Content/IND/Downloads/TFN_declaration_form_N3092.pdf) and Superannuation information.
1. [ ] New team member: Add your bank details in BambooHR (this must be done by day 1 or latest day 2).

</details>

<details>
<summary>New Team Member</summary>

1. [ ] People Ops: Double check that the `Employment Status` in BambooHR has two entries marked as `Probation Period` with date as the hire date and `probation period ends` with the end date as exactly 6 months from the hire date.

</details>
